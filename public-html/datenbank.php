<?php

// database connection code

// SQL Datenbank
$db_servername  = "db"; // Name in docker-compose.yml
$db_username    = "db_user";
$db_password    = "password";
$db_database    = "bierothek";

if(isset($_POST['beertype']))
{
    // Verbindung herstellen
    $conn = new mysqli($db_servername, $db_username, $db_password);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    // Datenbank anlegen
    // $conn->query("CREATE DATABASE $db_database");

    // Tabelle anlegen
    // $conn->query("CREATE TABLE $db_database.biersorten (name VARCHAR(20), ean VARCHAR(20), price decimal(5,2))");

    // Bier hinzufügen
    $beertype = $_POST['beertype'];
    $beerID   = $_POST['beerID'];
    $price    = $_POST['price'];
    $conn->query("INSERT INTO $db_database.biersorten (`name`,`ean`,`price`) VALUES ('$beertype',$beerID,$price)");

    // Rückgabe
    echo $beerID;
}

if (isset($_GET["q"])) {
    $ean = $_GET["q"];

    // Verbindung herstellen
    $conn = new mysqli($db_servername, $db_username, $db_password);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $result = $conn->query("SELECT * FROM $db_database.biersorten WHERE ean = $ean");
    
    if (mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_array($result);
        $name = $row['name'];
        echo $name;
    }

    mysqli_free_result($result);
}

?>
